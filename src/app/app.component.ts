import { CommonModule } from "@angular/common";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { Component, inject } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet,
    CommonModule,
    HttpClientModule
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  private readonly http = inject(HttpClient);

  data: any;

  constructor () {
    this.http
      .get<any[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(x => this.data = x);
  }

  removeItem(item: any) {
    //
  }

  getTitle(item: any) {
    return item.username + ' (' + item.email + ')';
  }
}
